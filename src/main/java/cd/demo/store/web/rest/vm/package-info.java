/**
 * View Models used by Spring MVC REST controllers.
 */
package cd.demo.store.web.rest.vm;
